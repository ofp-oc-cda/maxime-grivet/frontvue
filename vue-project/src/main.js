import { createApp } from 'vue'
import App from './App.vue'
import Index from './index.vue'

createApp(Index).mount('#app')